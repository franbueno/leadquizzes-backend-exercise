<?php

namespace AppBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Service\ImportService;

class AppFixtures extends Fixture
{

    public function __construct(ImportService $importService)
    {
        $this->importService = $importService;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->importService->importData();
    }
}
