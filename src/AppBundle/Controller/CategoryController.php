<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends FOSRestController
{
    /**
     * @Rest\Get("/api/category")
     */
    public function getAction()
    {
        $categories = $this->getDoctrine()->getRepository('AppBundle:Category')->findAll();

        if (!$categories) {
            return new View("There are no categories", Response::HTTP_NOT_FOUND);
        }

        return $categories;
    }
}
