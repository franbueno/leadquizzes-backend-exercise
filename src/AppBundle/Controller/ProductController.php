<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use AppBundle\Service\ProductService;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends FOSRestController
{
    /**
     * @var ProductService
     */
    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService  = $productService;
    }

    /**
     * @Rest\Get("/api/product")
     */
    public function getAction()
    {
        $products = $this->getDoctrine()->getRepository('AppBundle:Product')->findAll();

        if (!$products) {
            return new View("There are no products", Response::HTTP_NOT_FOUND);
        }

        return $products;
    }

    /**
     * @Rest\Get("/api/product/{id}")
     */
    public function idAction($id)
    {
        $product = $this->getDoctrine()->getRepository('AppBundle:Product')->find($id);

        if (!$product) {
            return new View("Product not found", Response::HTTP_NOT_FOUND);
        }

        return $product;
    }

    /**
     * @Rest\Post("/api/product")
     */
    public function postAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if ($this->productService->validate($data)) {
            $product = $this->productService->addProduct($data);

            if ($product) {
                return new View($product, Response::HTTP_CREATED);
            }
        }

        return new View("Malformed payload or wrong category id", Response::HTTP_NOT_FOUND);
    }

    /**
     * @Rest\Put("/api/product/{id}")
     */
    public function updateAction($id, Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if ($this->productService->validate($data))
        {
            $data['id'] = $id;
            $product = $this->productService->updateProduct($data);

            if ($product) {
                return new View($product, Response::HTTP_ACCEPTED);
            }
        }

        return new View("Malformed payload or wrong product/category id", Response::HTTP_NOT_FOUND);
    }

    /**
     * @Rest\Delete("/api/product/{id}")
     */
    public function deleteAction($id)
    {
        $data = $this->productService->deleteProduct($id);

        if ($data) {
            return new View('Product removed', Response::HTTP_OK);
        } else {
            return new View("Product not found", Response::HTTP_NOT_FOUND);
        }
    }

}
