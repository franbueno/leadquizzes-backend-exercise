<?php

namespace AppBundle\Service;

use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Optional;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Exception\ValidatorException;
use Psr\Log\LoggerInterface;

/**
 * Contains product related operations
 */
class ProductService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        EntityManagerInterface $entityManager,
        LoggerInterface $logger)
    {
        $this->entityManager = $entityManager;
        $this->logger        = $logger;
    }

    /**
     * @param [] $params - product details
     *
     * @return $product
     */

    public function addProduct(array $params)
    {
        // Get category by id
        $category = $this->entityManager->getRepository('AppBundle:Category')->find($params['category']);

        if (!$category) {
            $this->logger->error('Failed to add product. Category not found', [
                'category ID' => $params['category']
            ]);
            return null;
        }

        // Add product
        try {
            $product = new Product();
            $product->setName($params['name']);
            $product->setCategory($category);
            $product->setSku($params['sku']);
            $product->setPrice($params['price']);
            $product->setQuantity($params['quantity']);
            $product->setCreatedAt(new \DateTime());
            $product->setModifiedAt(new \DateTime());

            $this->entityManager->persist($product);
            $this->entityManager->flush();

            return $product;
        } catch(Exception $e) {
            $this->logger->error('Failed to add product.', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
            return null;
        }
    }

    /**
     * @param [] $params - products details
     *
     * @return Product
     */
    public function updateProduct($params)
    {
        $product = $this->entityManager->getRepository('AppBundle:Product')->find($params['id']);

        if (!$product) {
            $this->logger->error('Failed to update product. Product not found', [
                'product ID' => $params['id']
            ]);
            return null;
        }

        if(!empty($params['category'])) {
            // Get category by id
            $category = $this->entityManager->getRepository('AppBundle:Category')->find($params['category']);

            if (!$category) {
                $this->logger->error('Failed to update product. Category not found', [
                    'category ID' => $params['category']
                ]);
                return null;
            }
        }

        $product->setName($params['name'] ?? $product->getName());
        $product->setCategory($category);
        $product->setSKU($params['sku'] ?? $product->getSKU());
        $product->setPrice($params['price'] ?? $product->getPrice());
        $product->setQuantity($params['quantity'] ?? $product->getQuantity());
        $product->setModifiedAt(new \DateTime());

        $this->entityManager->persist($product);
        $this->entityManager->flush();

        return $product;
    }

    /**
     * @param int $id - product id
     *
     * @return void
     */
    public function deleteProduct($id)
    {
        $product = $this->entityManager->getRepository('AppBundle:Product')->find($id);

        if (!$product) {
            $this->logger->error('Failed to delete product. Product not found', [
                'product ID' => $id
            ]);
            return false;
        }

        $this->entityManager->remove($product);
        $this->entityManager->flush();

        return true;
    }

    /**
     * @param [] $params - products details
     * Helper function to validate input
     *
     */
    public function validate($params)
    {
        $constraints = new Collection([
            'name'     => new NotBlank(),
            'category' => new NotBlank(),
            'sku'      => new NotBlank(),
            'price'    => new NotBlank(),
            'quantity' => new NotBlank()
        ]);

        $validator = Validation::createValidator();
        $violations = $validator->validate($params, $constraints);

        if (count($violations) > 0) {
            return false;
        }

        return true;
    }
}
