<?php

namespace AppBundle\Service;

use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use AppBundle\Service\ProductService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ImportService
{
    const USER_MANAGER = 'fos_user.user_manager';

    /**
     * @var Container
     */
    private $container;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        EntityManagerInterface $entityManager,
        LoggerInterface $logger,
        UserPasswordEncoderInterface $encoder,
        Container $container)
    {
        $this->container = $container;
        $this->encoder = $encoder;
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    public function importData()
    {
        $posterList = file_get_contents('https://lq3-production.s3-us-west-2.amazonaws.com/coding-challenge/data-fixtures.json');
        $json = json_decode($posterList, true);

        // Create users
        if (count($json['users'])) {
            foreach ($json['users'] as $key => $value) {
                $fullName  = $value['name'];
                $fullNameArray = explode(" ", $fullName);

                // Lets get username from email
                $email  = $value['email'];
                $emailArray = explode("@", $email);

                $userManager = $this->container->get(static::USER_MANAGER);
                $user = $userManager->createUser();

                // Encode password
                $password = $this->encoder->encodePassword($user, 'password');

                $user->setUsername($emailArray[0]);
                $user->setEmail($value['email']);
                $user->setPassword($password);
                $user->setFirstName($fullNameArray[0]);
                $user->setLastName($fullNameArray[1]);
                $user->setEnabled(true);

                $this->entityManager->persist($user);
            }
        } else {
            $this->logger->error('Failed parsing users from url. Users not found');
        }

        // Check categories
        // Neccessary to save products with category_id
        $allCategories = array();
        foreach ($json['products'] as $key => $value) {
            array_push($allCategories, $value['category']);
        }
        // Clean repeated categories
        $categories = array_unique($allCategories);

        // Create categories
        if ($categories) {
            foreach ($categories as $key => $value) {
                $category = new Category();
                $category->setName($value);
                $category->setCreatedAt(new \DateTime());
                $category->setModifiedAt(new \DateTime());

                $this->entityManager->persist($category);
            }

            $this->entityManager->flush();
        } else {
            $this->logger->error('Failed parsing categories from url. Categories not found');
        }

        // Create products
        if (count($json['products'])) {
            foreach ($json['products'] as $key => $value) {
                $category = $this->entityManager
                    ->getRepository("AppBundle:Category")
                    ->findOneBy([
                        'name' => $value['category']
                    ]);

                $product = new product();
                $product->setName($value['name']);
                $product->setCategory($category);
                $product->setSKU($value['sku']);
                $product->setPrice($value['price']);
                $product->setQuantity($value['quantity']);
                $product->setCreatedAt(new \DateTime());
                $product->setModifiedAt(new \DateTime());

                $this->entityManager->persist($product);
            }
        } else {
            $this->logger->error('Failed parsing products from url. Products not found');
        }

        $this->entityManager->flush();
    }
}
